function estValide(button: HTMLButtonElement): boolean {
    return button.innerHTML.length === 0;
}

function setSymbol(btn: HTMLButtonElement, symbole: string): void {
    btn.innerHTML = symbole;
}

function rechercherVainqueur(pions: HTMLButtonElement[], joueurs: string[], tour: number): boolean {
    const conditionsGagnantes: number[][] = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8], // lignes
        [0, 3, 6], [1, 4, 7], [2, 5, 8], // colonnes
        [0, 4, 8], [2, 4, 6] // diagonales
    ];
    for (const condition of conditionsGagnantes) {
        const [a, b, c] = condition;
        if (pions[a].innerHTML === joueurs[tour] &&
            pions[b].innerHTML === joueurs[tour] &&
            pions[c].innerHTML === joueurs[tour]) {
            pions[a].style.backgroundColor = "#9ACD32";
            pions[b].style.backgroundColor = "#9ACD32";
            pions[c].style.backgroundColor = "#9ACD32";
            return true;
        }
    }
    return false;
}

function matchNul(pions: HTMLButtonElement[]): boolean {
    return Array.from(pions).every(pion => pion.innerHTML.length > 0);
}

interface Afficheur {
    sendMessage(message: string): void;
}

const Afficheur = function (element: HTMLElement): Afficheur {
    const affichage = element;
    function sendMessage(message: string): void {
        affichage.innerHTML = message;
    }
    return { sendMessage };
};

function main(): void {
    const pions = document.querySelectorAll<HTMLButtonElement>("#Jeu button");
    const joueurs = ["X", "O"];
    let tour = 0;
    let jeuEstFini = false;
    const afficheur = Afficheur(document.querySelector("#StatutJeu") as HTMLElement);
    afficheur.sendMessage(`Joueur ${joueurs[tour]} c'est à toi.`);

    pions.forEach(pion => {
        pion.addEventListener("click", function () {
            if (jeuEstFini) return;
            if (!estValide(this)) {
                afficheur.sendMessage(`Case occupée ! <br />Joueur ${joueurs[tour]} c'est toujours à toi !`);
            } else {
                setSymbol(this, joueurs[tour]);
                jeuEstFini = rechercherVainqueur(Array.from(pions), joueurs, tour);
                if (jeuEstFini) {
                    afficheur.sendMessage(`Le joueur ${joueurs[tour]} a gagné ! <br /> <a href="morpion.html">Rejouer</a>`);
                    return;
                }
                if (matchNul(Array.from(pions))) {
                    afficheur.sendMessage('Match Nul ! <br/> <a href="morpion.html">Rejouer</a>');
                    return;
                }
                tour = (tour + 1) % 2;
                afficheur.sendMessage(`Joueur ${joueurs[tour]} c'est à vous !`);
            }
        });
    });
}

main();
